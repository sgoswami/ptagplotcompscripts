#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 15:45:20 2022

@author: sgoswami
"""

tagger_list=[
    "rnnip",
    "rnnipflip",
    "DL1r",
    "DL1r20210824r22",
    "DL1dv00",
    "DL1d20210528r22",
    "DL1d20210824r22",
    "dips20210729",
    "dipsLoose20210729",
    "dipsLoose20220314v2",
    "dips20210729flip",
    "dipsLoose20210729flip",
    ]
tagger_list_rnnip=[
    "rnnip",
    "rnnipflip",
    ]
tagger_list_dl1=[ 
    "DL1r",
    "DL1r20210824r22",
    "DL1dv00",
    "DL1d20210528r22",
    "DL1d20210824r22",
    ]
tagger_list_dips=[
    "dips20210729",
    "dipsLoose20210729",
    "dipsLoose20220314v2",
    "dips20210729flip",
    "dipsLoose20210729flip",
    ]
