import numpy as np
import pandas as pd
import random
import h5py
from puma.utils import logger
from puma.metrics import calc_rej
from puma import Roc, RocPlot
from puma import VarVsEff, VarVsEffPlot
from taglist import *

logger.info("Starting pT vs EFF & Rejection plotting process ....")
logger.info("Reading h5 files")

###############################################################################
sig_eff = np.linspace(0.49, 1, 20)
###############################################################################
f_c_temp=[]
f_c_temp.append([0.070 for i in range(len(tagger_list_rnnip))])
f_c_temp.append([0.018 for i in range(len(tagger_list_dl1))])
f_c_temp.append([0.005 for i in range(len(tagger_list_dips))])

f_c = [element for sublist in f_c_temp for element in sublist]
#print(f_c)
###############################################################################
logger.info("This code produces pt vs EFF and pt vs rejection comparison plots for multiple p tags")
logger.info("Starting ROC plotting process.....")
logger.info("Reading h5 files......")
###############################################################################
base_addr="/home/sammy/data/comparisonplotstudies/"
base_list=[
    "ttbar/user.alfroch.410470.e6337_s3681_r13144_p4931.tdd.EMPFlow.2022-03-31T0524.2022-04-12-T112050_output.h5/user.alfroch.28682518._000010.output.h5",
    "ttbar/user.pgadow.410470.e6337_s3681_r13144_p5057.tdd.EMPFlow.22_2_75.r12684_r12782.v1-5-g5f61e27_output.h5/user.pgadow.29345477._000044.output.h5"
    ]
file_list= list(map(lambda m : base_addr + m, base_list))
print(len(file_list))
ptag=['p4931','p5057']
###############################################################################
jets=[]
is_l=[]
is_c=[]
is_b=[]
n_jets_l=[]
n_jets_c=[]
n_jets_b=[]
###############################################################################
for i in range(0,len(file_list)):
    with h5py.File(file_list[i]) as h5file:
        jets.append(pd.DataFrame(h5file["jets"][:3_00_000]))

pt=[]

for ptind in range(len(file_list)):
    pt.append(jets[ptind]["pt"]/1e3)

for i in range(0,len(file_list)):
    is_l.append(jets[i]["HadronConeExclTruthLabelID"]==0)
    is_c.append(jets[i]["HadronConeExclTruthLabelID"]==4)
    is_b.append(jets[i]["HadronConeExclTruthLabelID"]==5)

for i in range(0,len(file_list)):
    n_jets_l.append(sum(is_l[i]))
    n_jets_c.append(sum(is_c[i]))
    n_jets_b.append(sum(is_b[i]))
###############################################################################

disc=[]
for i in range(0,len(file_list)):
    sub=[]
    for index,tagger in enumerate(tagger_list):
        sub.append(np.apply_along_axis(
            lambda a: np.log(a[2] / ( f_c[index]*a[1]+(1-f_c[index])* a[0])),
            1,
            jets[i][[tagger+"_pu",tagger+"_pc",tagger+"_pb"]].values
            )
        )
    disc.append(sub)

###############################################################################
ujets_rej=[]
cjets_rej=[]
###############################################################################
for i in range(0,len(file_list)):
    sub1=[]
    sub2=[]
    for tagger in range(0,len(tagger_list)):
        sub1.append(calc_rej(
            disc[i][tagger][is_b[i]],
            disc[i][tagger][is_l[i]],
            sig_eff
            )
        )
        sub2.append(calc_rej(
            disc[i][tagger][is_b[i]],
            disc[i][tagger][is_c[i]],
            sig_eff
            )
        )
    ujets_rej.append(sub1)
    cjets_rej.append(sub2)

###############################################################################

for tagger in range(0,len(tagger_list)):
    refr=False
    name1=str(tagger_list[tagger])
    fcval=str(f_c[tagger])
    s1=f"Plotting $p_T$ vs Eff curves for tagger {name1}"
    st=f'\n$\\sqrt{{s}}=13$ TeV, PFlow,\n$t\\bar{{t}}$, $f_{{c}}={fcval}$'
    logger.info(s1)
    logger.info("Initializing plots as a function of pt.")
    plots_l=[]
    plots_c=[]
    plot_labels=[
    "p4974",
    "p5057",
    ]
    ######################################################################
    for i in range(len(file_list)):
        plots_l.append(
            VarVsEff(
                x_var_sig=pt[i][is_b[i]],
                disc_sig=disc[i][tagger][is_b[i]],
                x_var_bkg=pt[i][is_l[i]],
                disc_bkg=disc[i][tagger][is_l[i]],
                bins=[20, 30, 40, 60, 85, 110, 140, 200, 250,350,450,550,600],
                working_point=0.77,
                disc_cut=None,
                fixed_eff_bin=False,
                label=str(name1+" "+str(ptag[i])),
            )
        )
    ######################################################################
    for i in range(len(file_list)):
        plots_c.append(
            VarVsEff(
                x_var_sig=pt[i][is_b[i]],
                disc_sig=disc[i][tagger][is_b[i]],
                x_var_bkg=pt[i][is_c[i]],
                disc_bkg=disc[i][tagger][is_c[i]],
                bins=[20, 30, 40, 60, 85, 110, 140, 200, 250,350,450,550,600],
                working_point=0.77,
                disc_cut=None,
                fixed_eff_bin=False,
                label=str(name1+" "+str(ptag[i])),
            )
        )
    ####################################################################
    logger.info("Plotting light bkg rejection for inclusive efficiency as a function of pt.")
    # You can choose between different modes: "sig_eff", "bkg_eff", "sig_rej", "bkg_rej"
    plot_bkg_rej_l = VarVsEffPlot(
        n_ratio_panels=1,
        mode="bkg_rej",
        ylabel="Light jets rejection",
        xlabel=r"$p_{T}$ [GeV]",
        logy=False,
        atlas_first_tag= f"{name1}",
        atlas_second_tag= f'\n$\\sqrt{{s}}=13$ TeV, PFlow Jets,\n$t\\bar{{t}}$, $f_{{c}}={f_c[tagger]}$\n WP77',
    )
    ######################################################################
    for ctr in range(len(plots_l)):
        plot_bkg_rej_l.add(plots_l[ctr],reference=not bool(ctr))
        plot_bkg_rej_l.draw()
        plot_bkg_rej_l.savefig(f"{name1}_vs_pt_l_rej.png")

    ######################################################################
    logger.info("Plotting c bkg rejection for inclusive efficiency as a function of pt.")
    plot_bkg_rej_c = VarVsEffPlot(
            n_ratio_panels=1,
            mode="bkg_rej",
            ylabel="c jets rejection",
            xlabel=r"$p_{T}$ [GeV]",
            logy=False,
            atlas_first_tag= f"{name1}",
            atlas_second_tag= f'\n$\\sqrt{{s}}=13$ TeV, PFlow Jets,\n$t\\bar{{t}}$, $f_{{c}}={f_c[tagger]}$\n WP77',
    )
    ######################################################################
    for ctr in range(len(plots_c)):
        plot_bkg_rej_c.add(plots_c[ctr],reference=not bool(ctr))
        plot_bkg_rej_c.draw()
        plot_bkg_rej_c.savefig(f"{name1}_vs_pt_c_rej.png")
    ######################################################################
    plot_sig_eff = VarVsEffPlot(
        n_ratio_panels=1,
        mode="sig_eff",
        ylabel="b-jets efficiency",
        xlabel=r"$p_{T}$ [GeV]",
        logy=False,
        atlas_first_tag= f"{name1}",
        atlas_second_tag= f'\n$\\sqrt{{s}}=13$ TeV, PFlow Jets,\n$t\\bar{{t}}$, $f_{{c}}={f_c[tagger]}$\n WP77'
    )

    for ctr in range(len(plots_c)):
        plot_sig_eff.add(plots_l[ctr], reference=not bool(ctr))

    # If you want to inverse the discriminant cut you can enable it via
    # plot_sig_eff.set_inverse_cut()
    plot_sig_eff.draw()
    # Drawing a hline indicating inclusive efficiency
    plot_sig_eff.draw_hline(0.77)
    plot_sig_eff.savefig(f"pt_b_eff_{name1}.png")
